
$(function () {


    $('.addSubmenu').on('click', function () {
        $('#titleModalLabel').html('Add New Sub Menu');
        $('.modal-footer button[type=submit]').html('Add');
        $('.modal-content form').attr('action', 'http://localhost/bedstatus/menu/submenu');
        $('#title').val("");
        $('#url').val("");
        $('#icon').val("");
        $('#menu_id #option1').text("Select Menu").attr('value', "");
        $('.modal-body #is_active').attr('checked', true);

    });

    $('.modalEdit').on('click', function () {
        $('#titleModalLabel').html('Edit Submenu');

        $('.modal-footer button[type=submit]').html('Edit');
        //belum beres
        $('.modal-content form').attr('action', 'http://localhost/bedstatus/menu/editsubmenu');
        const id = $(this).data('id');
        $.ajax({
            url: 'http://localhost/bedstatus/menu/getsubmenubyid/',
            data: { id: id },
            method: 'POST',
            dataType: 'json',
            success: function (data) {

                // console.log(data);

                $('#title').val(data.title);
                $('#url').val(data.url);
                $('#icon').val(data.icon);
                $('#id').val(data.id);
                $('#menu_id #option1').text(data.menu).attr('value', data.menu_id);


                if (data.is_active != 1) {
                    // console.log("betul");
                    $('.modal-body #is_active').attr('checked', false);
                } else {
                    // console.log("salah");
                    $('.modal-body #is_active').attr('checked', true);
                }

            }

        });

    });


    $('.addBed').on('click', function () {
        $('#titleModalLabel').html('Tambah Bed');
        $('.modal-footer button[type=submit]').html('Add');
        $('.modal-content form').attr('action', 'http://localhost/bedstatus/bed/addbed');
        $('#id').val("");
        $('#kelas').val("");
        $('#kode_ruang').val("");
        $('#nama_ruang').val("");
        $('#kapasitas').val("0");
        $('#tersedia').val("0");
        $('#tersedia_p').val("0");
        $('#tersedia_w').val("0");
        $('#tersedia_pw').val("0");
        $('#dipakai').val("0");

    });

    $('.Modaledit').on('click', function () {
        $('.modal-footer button[type=submit]').html('Edit');
        $('#titleModalLabel').html('Edit Bed');
        $('.modal-content form').attr('action', 'http://localhost/bedstatus/bed/updatebed');
        const id = $(this).data('id');
        $.ajax({
            url: 'http://localhost/bedstatus/bed/getbedByid/',
            data: { id: id },
            method: 'POST',
            dataType: 'json',
            success: function (data) {


                // console.log(data);

                $('#id').val(data.id);
                $('#kelas').val(data.kelas);
                $('#kode_ruang').val(data.kode_ruang);
                $('#nama_ruang').val(data.nama_ruang);
                $('#kapasitas').val(data.kapasitas);
                $('#tersedia').val(data.tersedia);
                $('#tersedia_p').val(data.tersedia_pria);
                $('#tersedia_w').val(data.tersedia_wanita);
                $('#tersedia_pw').val(data.tersedia_pw);
                $('#dipakai').val(data.dipakai);


            }

        });

    });




    $('#date_time').ngDateTime();


});

