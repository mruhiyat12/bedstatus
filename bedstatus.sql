-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 10, 2021 at 08:53 AM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bedstatus`
--

-- --------------------------------------------------------

--
-- Table structure for table `bedrs`
--

CREATE TABLE `bedrs` (
  `id` int(11) NOT NULL,
  `kode_ruang` varchar(255) NOT NULL,
  `nama_ruang` varchar(255) NOT NULL,
  `kelas` varchar(255) NOT NULL,
  `kapasitas` int(11) NOT NULL,
  `tersedia` int(11) NOT NULL,
  `tersedia_pria` int(11) NOT NULL,
  `tersedia_wanita` int(11) NOT NULL,
  `tersedia_pw` int(11) NOT NULL,
  `dipakai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bedrs`
--

INSERT INTO `bedrs` (`id`, `kode_ruang`, `nama_ruang`, `kelas`, `kapasitas`, `tersedia`, `tersedia_pria`, `tersedia_wanita`, `tersedia_pw`, `dipakai`) VALUES
(25, '-', 'RAWAT INAP', 'SUPER VIP', 4, 4, 0, 0, 0, 0),
(26, '-', 'RAWAT INAP', 'VIP', 7, 5, 0, 0, 0, 2),
(27, '-', 'RAWAT INAP', 'KELAS 1', 11, 10, 0, 0, 0, 1),
(28, '-', 'ICU', 'ICU', 2, 2, 0, 0, 0, 0),
(29, '-', 'ICU ISOLASI', 'ICU ISOLASI', 5, 5, 0, 0, 0, 0),
(30, '-', 'NICU', 'NICU/PICU', 2, 1, 0, 0, 0, 1),
(31, '-', 'NICU ISOLASI', 'NICU/PICU', 3, 3, 0, 0, 0, 0),
(32, '-', 'HCU ISOLASI', 'HCU ISOLASI', 2, 2, 0, 0, 0, 0),
(33, '-', 'RAWAT INAP', 'KAMAR ISOLASI', 56, 56, 0, 0, 0, 0),
(34, '-', 'PERINA BAYI SEHAT', 'KELAS III', 14, 13, 0, 0, 0, 1),
(35, '-', 'PERINATOLOGI', 'PERINATOLOGI', 4, 4, 0, 0, 0, 0),
(36, '-', 'PERINA BAYI SEHAT ISOLASI', 'PERINA BAYI SEHAT ISOLASI', 3, 3, 0, 0, 0, 0),
(37, '-', 'PERINATOLOGI ISOLASI', 'PERINATOLOGI ISOLASI', 2, 2, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`) VALUES
(1, 'Muhammad Ruhiyat', 'muhammadruhiyat020998@gmail.com', 'image.png', '$2y$10$DXXYht5NKgF3CGbY0l8mNOe1t87XUmOyokHIld77NrBar1Bove6La', 1, 1, 1603241365),
(2, 'dani', 'dani@gmail.com', 'default.jpg', '$2y$10$zcKO4UotAjik1KnAdIi1hOd8IBntZ1TKPD0PjkKXIYTY2r89L75R6', 2, 1, 1603276157),
(10, 'Muhammad Ruhiyat', 'muhammadruhiyat06@gmail.com', 'default.jpg', '$2y$10$xL7VcDSSSkrrxZ6gbjlGhu7NnDaIdvLmqeFm9sAYRUvdQXpro56pS', 2, 1, 1608076244),
(11, 'admin', 'itrspkrw@gmail.com', 'default.jpg', '$2y$10$EEnvVSutNP4FAhQl7QcGc.WA4z7FPKlJGPL8frDcrJxca9Vl5b5GG', 2, 0, 1631256467);

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`) VALUES
(7, 1, 2),
(14, 1, 4),
(15, 2, 4),
(19, 2, 2),
(20, 1, 3),
(21, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`) VALUES
(1, 'Admin'),
(2, 'User'),
(3, 'Menu'),
(4, 'Bed');

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`id`, `role`) VALUES
(1, 'Administrator'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`) VALUES
(3, 2, 'Edit Profile', 'user/edit', 'fas fa-fw fa-user-edit', 1),
(4, 3, 'Menu Management', 'menu', 'fas fa-fw fa-folder', 1),
(5, 3, 'Submenu Management', 'menu/submenu', 'fas fa-fw fa-folder-open', 1),
(7, 2, 'Change Password', 'user/changepassword', 'fas fa-fw fa-key', 1),
(10, 2, 'My Profile', 'user', 'fas fa-fw fa-user-circle', 1),
(14, 1, 'Dashboard', 'admin', 'fa fa-fw fa-tachometer-alt1', 1),
(15, 1, 'Role', 'admin/role', 'fa fa-fw fa-user-tag', 1),
(16, 1, 'Account', 'admin/account_user', 'fa fa-fw fa-users', 1),
(17, 4, 'Entri Bed', 'bed/entri', 'fas fa-fw fa-bed', 1),
(18, 4, 'View Bed Status', 'bedview/view', 'fas fa-fw fa-eye', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_token`
--

INSERT INTO `user_token` (`id`, `email`, `token`, `date_created`) VALUES
(1, 'itrspkrw@gmail.com', 'yCYFKnt5gNqCOeeGdORF/DcXvf4fb9iwepgYmRqj3lc=', 1631256467);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bedrs`
--
ALTER TABLE `bedrs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Indexes for table `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_menu` (`menu_id`);

--
-- Indexes for table `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bedrs`
--
ALTER TABLE `bedrs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD CONSTRAINT `menu_id` FOREIGN KEY (`menu_id`) REFERENCES `user_menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `role_id` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD CONSTRAINT `sub_menu` FOREIGN KEY (`menu_id`) REFERENCES `user_menu` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
