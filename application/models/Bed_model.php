<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bed_model extends CI_Model
{
    public function Getallbed()
    {
        return $this->db->get('bedrs')->result_array();
    }

    public function Addbed()

    {
        if ($this->input->post('kapasitas') == null) {
            $kapasitas = 0;
        } else {
            $kapasitas = $this->input->post('kapasitas');
        }

        if ($this->input->post('tersedia') == null) {
            $tersedia = 0;
        } else {
            $tersedia = $this->input->post('tersedia');
        }
        if ($this->input->post('tersedia_p') == null) {
            $tersedia_p = 0;
        } else {
            $tersedia_p = $this->input->post('tersedia_p');
        }
        if ($this->input->post('tersedia_w') == null) {
            $tersedia_w = 0;
        } else {
            $tersedia_w = $this->input->post('tersedia_w');
        }
        if ($this->input->post('tersedia_pw') == null) {
            $tersedia_pw = 0;
        } else {
            $tersedia_pw = $this->input->post('tersedia_pw');
        }
        if ($this->input->post('dipakai') == null) {
            $dipakai = 0;
        } else {
            $dipakai = $this->input->post('dipakai');
        }


        $data = [

            'kode_ruang' => $this->input->post('kode_ruang'),
            'nama_ruang' => $this->input->post('nama_ruang'),
            'kelas' => $this->input->post('kelas'),
            'kapasitas' => $kapasitas,
            'tersedia' => $tersedia,
            'tersedia_pria' => $tersedia_p,
            'tersedia_wanita' => $tersedia_w,
            'tersedia_pw' => $tersedia_pw,
            'dipakai' => $dipakai
        ];

        return $this->db->insert('bedrs', $data);
    }

    public function Getbedbyid($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('bedrs')->row_array();
    }

    public function Update()
    {
        $data = [

            'kode_ruang' => $this->input->post('kode_ruang'),
            'nama_ruang' => $this->input->post('nama_ruang'),
            'kelas' => $this->input->post('kelas'),
            'kapasitas' => $this->input->post('kapasitas'),
            'tersedia' => $this->input->post('tersedia'),
            'tersedia_pria' => $this->input->post('tersedia_p'),
            'tersedia_wanita' => $this->input->post('tersedia_w'),
            'tersedia_pw' => $this->input->post('tersedia_pw'),
            'dipakai' => $this->input->post('dipakai')
        ];
        $id = ['id' => $this->input->post('id')];

        $this->db->where($id);
        $this->db->update('bedrs', $data);
    }

    public function Hapus($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('bedrs');
    }

    public function Bedall()
    {
        return $this->db->get('bedrs')->row_array();
    }
}
