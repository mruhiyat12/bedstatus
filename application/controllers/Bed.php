<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bed extends CI_Controller
{
    public function __construct()
    {

        parent::__construct();
        check_login();
    }

    public function entri()
    {

        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();
        $data['bed'] = $this->Bed_model->Getallbed();
        $data['title'] = 'Entri Bed';
        // echo 'selamat datang ' . $data['user']['name'];
        $this->load->view('templates/header', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('templates/topbar', $data);
        $this->load->view('bedstatus/index', $data);
        $this->load->view('templates/footer');
    }

    public function addbed()
    {
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Bed Berhasil Ditambahkan!</div>');
        $this->Bed_model->Addbed();
        redirect('bed/entri');
    }

    public function getbedByid()
    {
        $id =  $this->input->post('id');
        echo json_encode($this->Bed_model->Getbedbyid($id));
    }

    public function updatebed()
    {

        $this->Bed_model->Update();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Update Succes!</div>');
        redirect('bed/entri');
    }

    public function hapus($id)
    {
        $this->Bed_model->Hapus($id);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">Bed Berhasil dihapus!</div>');
        redirect('bed/entri');
    }
}
