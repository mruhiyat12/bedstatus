<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Bedview extends CI_Controller
{
    // public function __construct()
    // {

    //     parent::__construct();
    //     check_login();
    // }

    public function view()
    {
        $data['user'] = $this->db->get_where('user', ['email' => $this->session->userdata('email')])->row_array();


        $data['title'] = 'BED STATUS';
        // echo 'selamat datang ' . $data['user']['name'];
        $this->load->view('templates/header', $data);
        // $this->load->view('templates/sidebar', $data);
        // $this->load->view('templates/topbar', $data);
        $this->load->view('bedstatus/view');
        $this->load->view('templates/footer');
    }

    public function load()
    {
        $data['view'] = $this->Bed_model->Getallbed();
        $this->load->view('bedstatus/loadfile', $data);
    }
}
