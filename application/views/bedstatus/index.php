<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->


    <div class="shadow card text-dark bg-light mb-3">
        <div class="card-header">
            <h1 class="h3 mb-4 text-gray-800" style="text-transform: uppercase;"><?= $title; ?></h1>
        </div>
        <div class="card-body">
            <?= $this->session->flashdata('message'); ?>
            <a href="" class="btn btn-primary mb-3 addBed" data-toggle="modal" data-target="#formModal">Tambah Kamar</a>

            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">KODE RUANGAN</th>
                        <th scope="col">Nama Ruangan</th>
                        <th scope="col">Kelas</th>
                        <th scope="col">Kapasitas</th>
                        <th scope="col">Tersedia</th>
                        <th scope="col">Pilihan</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($bed as $b) : ?>
                        <tr>
                            <th><?= $b['kode_ruang']; ?></th>
                            <td><?= $b['nama_ruang']; ?></td>
                            <td><?= $b['kelas']; ?></td>
                            <td><?= $b['kapasitas']; ?></td>
                            <td><?= $b['tersedia']; ?></td>
                            <td>
                                <a href="#" class="badge badge-success Modaledit" data-toggle="modal" data-target="#formModal" data-id="<?= $b['id']; ?>">Edit</a>
                                <a href="<?= base_url('bed/hapus/') . $b['id'];  ?>" class="badge badge-danger">Delete</a>
                            </td>
                        </tr>
                    <?php endforeach; ?>

                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->



<!-- Modal  -->


<div class="modal fade" id="formModal" tabindex="-1" aria-labelledby="titleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="titleModalLabel"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="POST">
                <input type="hidden" name="id" id="id">
                <div class="modal-body">
                    <label for="basic-url" class="form-label">Kelas</label>
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" id="kelas" name="kelas">
                    </div>
                    <label for="basic-url" class="form-label">Kode Ruangan</label>
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" id="kode_ruang" name="kode_ruang">

                    </div>
                    <label for="basic-url" class="form-label">Nama Ruangan</label>
                    <div class="form-group mb-3">
                        <input type="text" class="form-control" id="nama_ruang" name="nama_ruang">

                    </div>
                    <label for="basic-url" class="form-label">Kapasitas</label>
                    <div class="form-group mb-3">
                        <input type="number" class="form-control" id="kapasitas" name="kapasitas">

                    </div>
                    <label for="basic-url" class="form-label">Tersedia</label>
                    <div class="form-group mb-3">
                        <input type="number" class="form-control" id="tersedia" name="tersedia">

                    </div>
                    <label for="basic-url" class="form-label">Tersedia Pria</label>
                    <div class="form-group mb-3">
                        <input type="number" class="form-control" id="tersedia_p" name="tersedia_p">

                    </div>

                    <label for="basic-url" class="form-label">Tersedia Wanita</label>
                    <div class="form-group mb-3">
                        <input type="number" class="form-control" id="tersedia_w" name="tersedia_w">

                    </div>
                    <label for="basic-url" class="form-label">Tersedia Pria Wanita</label>
                    <div class="form-group mb-3">
                        <input type="number" class="form-control" id="tersedia_pw" name="tersedia_pw">

                    </div>
                    <label for="basic-url" class="form-label">Dipakai</label>
                    <div class="form-group mb-3">
                        <input type="number" class="form-control" id="dipakai" name="dipakai">

                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary"></button>
                </div>
            </form>
        </div>
    </div>
</div>