<!-- Begin Page Content -->
<div class="container-fluid">

    <!-- Page Heading -->
    <!-- Page Heading -->
    <div class="row ">
        <div class="col-md-6 mb-4 " style="padding-top: 2%;">
            <h1 style=" color: black;"><?= $title; ?></h1>
        </div>
        <div class="col-md-6 mb-4 " style="padding-top: 2%; padding-left: 30%;">
            <img src="<?= base_url('assets/img/LOGO RSPK BARU NO ALAMAT.png'); ?>" alt="RS. Permata Keluarga Karawang" style="height: 50px; ">
        </div>


    </div>


    <div class="shadow card text-dark bg-light mb-3 ">
        <div class="row" id="date_time" style="padding-left: 2%; padding-top: 2%; font-size: 25px; color: black;">
            <p class="col-md-6" id="time"></p>
            <p class="col-md-6" style="padding-left: 20%;" id="date"></p>
        </div>
        <div class="card-body">
            <table class="table table-striped" style="color: black; font-size: 25px; text-align: center;">
                <thead>
                    <tr>
                        <th scope="col">KAMAR</th>
                        <th scope="col">KELAS</th>
                        <th scope="col">KAPASITAS</th>
                        <th scope="col">TERSEDIA</th>
                        <th scope="col">PRIA</th>
                        <th scope="col">PEREMPUAN</th>
                        <th scope="col">DIPAKAI</th>
                    </tr>
                </thead>
                <tbody id="view">

                </tbody>
            </table>
        </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<script type="text/javascript">
    setInterval(function() {
        $("#view").load("<?= base_url('bedview/load'); ?>");


    }, 1000);
</script>